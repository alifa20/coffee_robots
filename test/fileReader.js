const fs = require('fs');
const path = require('path');

const expect = require('chai').expect;
const fileReader = require('../src/fileReader');
var fr;

describe('fileReader', function() {
    before(function() {
        fr = new fileReader({ filename: 'test/valid.txt' });
    });

    describe('using 1 robot', function() {

        it('should count 2 robots', function() {
            var input = "5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMLMLMRMRMRRM";
            var c = fr.robotCount(input.split('\n'));
            expect(c).to.be.equal(2);
        });

    });

    describe('using 2 robots', function() {
        it('should convert input to 2 robot objects', function() {
            var input = "5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMLMLMRMRMRRM";
            var robots = fr.convertLinesToRobotsObj(input.split('\n'));
            expect(robots.length).to.be.equal(2);
        });
    });
});