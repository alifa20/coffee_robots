const fs = require('fs');
const path = require('path');
const expect = require('chai').expect;
const robotsMove = require('../src/robotsMove');

var robots_move_obj = new robotsMove();
var robot1 = { x: 1, y: 2, heading: 'N', commands: 'LMLMLMLMM', shop: { height: 5, width: 5 } };
var robot2 = { x: 3, y: 3, heading: 'E', commands: 'MLMLMRMRMRRM', shop: { height: 5, width: 5 } };

describe('robotsMove', function() {
    it('should show the longest command', function() {
        var longestCommand = robots_move_obj.longestCommand([robot1, robot2]);
        expect(longestCommand).to.be.equal('MLMLMRMRMRRM');
    });
    describe('Robots turn and move', function() {
        it('should turn left', function() {
            var turned_robot = robots_move_obj.turnLeft(robot1);
            expect(turned_robot.heading).to.be.equal('W');
            turned_robot = robots_move_obj.turnLeft(turned_robot);
            expect(turned_robot.heading).to.be.equal('S');
            turned_robot = robots_move_obj.turnLeft(turned_robot);
            expect(turned_robot.heading).to.be.equal('E');
            turned_robot = robots_move_obj.turnLeft(turned_robot);
            expect(turned_robot.heading).to.be.equal('N');
        });
        it('should turn right', function() {
            var turned_robot = robots_move_obj.turnRight(robot2);
            expect(turned_robot.heading).to.be.equal('S');
            turned_robot = robots_move_obj.turnRight(turned_robot);
            expect(turned_robot.heading).to.be.equal('W');
            turned_robot = robots_move_obj.turnRight(turned_robot);
            expect(turned_robot.heading).to.be.equal('N');
            turned_robot = robots_move_obj.turnRight(turned_robot);
            expect(turned_robot.heading).to.be.equal('E');
        });

        it('should move forward', function() {
            var moved_robot = robots_move_obj.moveForward(robot1);
            expect(moved_robot.y).to.be.equal(1);
            moved_robot.heading = 'E';
            moved_robot = robots_move_obj.moveForward(robot1);
            expect(moved_robot.x).to.be.equal(2);
            moved_robot.heading = 'S';
            moved_robot = robots_move_obj.moveForward(robot1);
            expect(moved_robot.y).to.be.equal(2);
            moved_robot.heading = 'W';
            moved_robot = robots_move_obj.moveForward(robot1);
            expect(moved_robot.x).to.be.equal(1);
        });
    });

    describe('Update all robots\' locations', function() {
        it('should move all robots', function() {
            var rb1 = { x: 1, y: 2, heading: 'N', commands: 'LMLM', shop: { height: 5, width: 5 } };
            var rb2 = { x: 3, y: 3, heading: 'E', commands: 'MLMLM', shop: { height: 5, width: 5 } };
            robots = [rb1, rb2];
            var updated_robots = robots_move_obj.updateLocationAll(robots);
            robots = updated_robots.robots;
            expect(updated_robots.hasclash).to.be.equal(false);
            expect(robots[0].x).to.be.equal(0);
            expect(robots[0].y).to.be.equal(3);
            expect(robots[0].heading).to.be.equal('S');
            expect(robots[1].x).to.be.equal(3);
            expect(robots[1].y).to.be.equal(2);
            expect(robots[1].heading).to.be.equal('W');
        });

        it('should move all robots2', function() {
            var rb1 = { x: 1, y: 2, heading: 'N', commands: 'LMLMLMLMM', shop: { height: 5, width: 5 } };
            var rb2 = { x: 3, y: 3, heading: 'E', commands: 'MLMLMRMRMRRM', shop: { height: 5, width: 5 } };
            robots = [rb1, rb2];
            // robots = robots_move_obj.updateLocationAll(robots);
           var updated_robots = robots_move_obj.updateLocationAll(robots);
            robots = updated_robots.robots;
            expect(updated_robots.hasclash).to.be.equal(false);
            
            expect(robots[0].x).to.be.equal(1);
            expect(robots[0].y).to.be.equal(1);
            expect(robots[0].heading).to.be.equal('N');
            expect(robots[1].x).to.be.equal(3);
            expect(robots[1].y).to.be.equal(1);
            expect(robots[1].heading).to.be.equal('W');
        });
        it('should not move off the board', function() {
            var rb1 = { x: 1, y: 2, heading: 'N', commands: 'MLM', shop: { height: 5, width: 5 } };
            var rb2 = { x: 3, y: 3, heading: 'E', commands: 'MLMLM', shop: { height: 5, width: 5 } };

            robots = [rb1, rb2];
            // robots = robots_move_obj.updateLocationAll(robots);
            var updated_robots = robots_move_obj.updateLocationAll(robots);
            robots = updated_robots.robots;
            expect(updated_robots.hasclash).to.be.equal(false);
            expect(robots[0].x).to.be.equal(0);
            expect(robots[0].y).to.be.equal(1);
        });

        it('should not move off the board', function() {
            var rb1 = { x: 4, y: 0, heading: 'N', commands: 'MLM', shop: { height: 5, width: 5 } };
            var rb2 = { x: 3, y: 3, heading: 'E', commands: 'MLMLM', shop: { height: 5, width: 5 } };
            robots = [rb1, rb2];
            var updated_robots = robots_move_obj.updateLocationAll(robots);
            robots = updated_robots.robots;
            expect(updated_robots.hasclash).to.be.equal(false);
            expect(robots[0].x).to.be.equal(3);
            expect(robots[0].y).to.be.equal(0);
        });

        it('should report clash', function() {
            var rb1 = { x: 2, y: 2, heading: 'W', commands: 'M', shop: { height: 5, width: 5 } };
            var rb2 = { x: 0, y: 2, heading: 'E', commands: 'M', shop: { height: 5, width: 5 } };
            robots = [rb1, rb2];
            var updated_robots = robots_move_obj.updateLocationAll(robots);
            robots = updated_robots.robots;
            expect(updated_robots.hasclash).to.be.equal(true);
            expect(robots[0].x).to.be.equal(2);
            expect(robots[0].y).to.be.equal(2);
            expect(robots[1].x).to.be.equal(0);
            expect(robots[1].y).to.be.equal(2);
        });

    })
});