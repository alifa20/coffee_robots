var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var shopModel = new Schema({
    width: {
        type: Number
    },
    height: {
        type: Number
    }
});

module.exports = mongoose.model('shops', shopModel);