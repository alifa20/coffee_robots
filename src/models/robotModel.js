var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var robotModel = new Schema({
    x: {
        type: Number
    },
    y: {
        type: Number
    },
    heading: {
        type: String
    },
    commands: {
        type: String
    },
    shopId: {
        type: Schema.ObjectId
    }
});


module.exports = mongoose.model('robots', robotModel);