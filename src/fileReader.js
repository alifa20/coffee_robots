// Read the file and print its contents.
var fs = require('fs');
const Robot = require('./models/robotModel');
const Shop = require('./models/shopModel');


var FileReader = function(args) {
    this.readFile = function(fn) {
        fs.readFile(args.filename, 'utf8', function(err, data) {
            if (err) throw err;
            fn(data);
        });
    };
    this.robotCount = function(lines) {
        return (lines.length - 1) / 2;
    };

    this.convertLinesToRobotsObj = function convertLinesToRobotsObj(lines) {
        var count = this.robotCount(lines);
        var robots = [];
        var robot;
        var shop = {
            width: parseInt(lines[0].split(" ")[0]),
            height: parseInt(lines[0].split(" ")[1]),
        };
        for (var i = 0; i < count; i++) {
            var robot_line = lines.slice((i * 2) + 1);
            robot = {
                x: parseInt(robot_line[0].split(" ")[0]),
                y: parseInt(robot_line[0].split(" ")[1]),
                heading: robot_line[0].split(" ")[2],
                commands: robot_line[1],
                shop: shop
            };
            robots.push(robot);
        }
        return robots;
    };
};

module.exports = FileReader;