var RobotsMove = function() {
    this.next = function next(params) {

    };
    this.longestCommand = function(robots) {
        var longestCommand = robots.map(function(o) { return o.commands; }).reduce(function(a, b) { return a.length > b.length ? a : b; });
        return longestCommand;
    };

    this.updateLocationAll = function move(robots) {

        // find longest command
        var longestCommand = this.longestCommand(robots);
        var initial_robots = robots;
        if (this.hasClash(robots)) {
            console.log('clash happened!');
            return { hasclash: true, robots: initial_robots };
        }
        for (var i = 0; i < longestCommand.length; i++) {
            for (var j = 0; j < robots.length; j++) {
                if (robots[j].commands.length <= i) {
                    continue;
                }
                robots[j] = this.updateLocation(robots[j], robots[j].commands[i]);
            }
            if (this.hasClash(robots)) {
                console.log('clash happened!');
                return { hasclash: true, robots: initial_robots };
            }
        }
        return { hasclash: false, robots: robots };
    }

    this.hasClash = function checkClash(robots) {
        var map = {}, i, size, key;

        for (i = 0, size = robots.length; i < size; i++) {
            key = String(robots[i].x) + String(robots[i].y);
            if (map[key]) {
                return true;
            }
            map[key] = true;
        }

        return false;
    };

    this.updateLocation = function updateLocation(robot, nextstep) {
        switch (nextstep) {
            case 'L':
                return this.turnLeft(robot);
            case 'R':
                return this.turnRight(robot);
            case 'M':
                return this.moveForward(robot);
            default:
                return robot;
        }
    };

    this.moveForward = function move(robot) {
        if (robot.heading === 'N' && robot.y > 0) {
            robot.y -= 1;
            return robot;
        }
        if (robot.heading === 'W' && robot.x > 0) {
            robot.x -= 1;
            return robot;
        }
        if (robot.heading === 'S' && (robot.y + 1) < robot.shop.height) {
            robot.y += 1;
            return robot;
        }
        if (robot.heading === 'E' && (robot.x + 1) < robot.shop.width) {
            robot.x += 1;
            return robot;
        }
        return robot;
    }

    this.turnLeft = function turnLeft(robot) {
        if (robot.heading === 'N') {
            robot.heading = 'W';
            return robot;
        }
        if (robot.heading === 'W') {
            robot.heading = 'S';
            return robot;
        }
        if (robot.heading === 'S') {
            robot.heading = 'E';
            return robot;
        }
        if (robot.heading === 'E') {
            robot.heading = 'N';
            return robot;
        }
    };

    this.turnRight = function turnRight(robot) {
        if (robot.heading === 'N') {
            robot.heading = 'E';
            return robot;
        }
        if (robot.heading === 'W') {
            robot.heading = 'N';
            return robot;
        }
        if (robot.heading === 'S') {
            robot.heading = 'W';
            return robot;
        }
        if (robot.heading === 'E') {
            robot.heading = 'S';
            return robot;
        }
    };
};

module.exports = RobotsMove;