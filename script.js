#!/usr/bin/env node

const fs = require('fs')
const fileReader = require('./src/fileReader');
const robotsMove = require('./src/robotsMove');

function usage() {
    console.error([
        'usage: script.js <file>',
        '',
        '<file> - txt file containing robots info'
    ].join('\n'))
}
module.exports.usage = usage;

const main = module.exports.main = function main(outStream) {
    if (process.argv.length !== 3) {
        module.exports.usage()
        process.exit(1)
        return
    }
    fr = new fileReader({ filename: process.argv[2] });
    robots_move_obj = new robotsMove();
    var robots = [];
    fr.readFile(function(data) {
        var lines = data.split('\n');
        robots = fr.convertLinesToRobotsObj(lines);
        var updated_robots = robots_move_obj.updateLocationAll(robots);
        robots = updated_robots.robots;
        if (updated_robots.hasclash) {
            console.log('Clash happens!');
        }
        console.log(robots);

    });
}

if (require.main === module) {
    main(process.stdout)
}