const fs = require('fs')
const fileReader = require('./src/fileReader');
const robotsMove = require('./src/robotsMove');

var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');

var app = express();

var port = process.env.PORT || 3000;


var db = mongoose.connect(process.env.MONGO_URL || 'mongodb://localhost/heyyou');

var Shop = require('./src/models/shopModel');
var Robot = require('./src/models/robotModel');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var appRouter = express.Router();

appRouter.route('/shop')
    .post(function(req, res) {
        var shop = new Shop(req.body);
        shop.save();
        res.send(shop);
    });

appRouter.route('/shops')
    .get(function(req, res) {
        Shop.find(function(err, shops) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                res.json(shops);
            }
        });
    });

appRouter.route('/robots')
    .get(function(req, res) {
        Robot.find(function(err, robots) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                res.json(robots);
            }
        });
    });
appRouter.route('/shop/:id')
    .get(function(req, res) {
        Shop.findById(req.params.id, function(err, shop) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                res.json(shop);
            }
        });
    })
    .delete(function(req, res) {
        console.log('in delete');
        Shop.findById(req.params.id, function(err, shop) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                try {
                    shop.remove(function(err) {
                        if (err) {
                            res.status(500).send(err);
                        }
                        else {
                            var ret = ""
                            res.json({ status: "ok" });
                        }
                    });
                } catch (error) {
                    res.status(500).send(error);
                }
            }
        });
    });


appRouter.route('/shop/:id/robots')
    .get(function(req, res) {
        Shop.findById(req.params.id, function(err, shop) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                Robot.find({ shop: shop._id }, function(err, robots) {
                    if (err) {
                        res.status(500).send(err);
                    }
                    else { res.send(robots); }
                });
            }
        });
    });

appRouter.route('/shop/:id/robot')
    .post(function(req, res) {
        console.log('robot insert');
        Shop.findById(req.params.id, function(err, shop) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                var robot = new Robot(req.body);
                robot.shopId = req.params.id;
                robot.save();
                res.send(robot);
            }
        });
    });

appRouter.route('/shop/:id/robot/:rid')
    .put(function(req, res) {
        console.log('robot updating');
        Shop.findById(req.params.id, function(err, shop) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                Robot.findById(req.params.rid, function(err, robot) {
                    robot.x = req.body.x;
                    robot.y = req.body.y;
                    robot.heading = req.heading;
                    robot.commands = req.commands;
                    robot.save();
                    res.send(robot);
                });
            }
        });
    })
    .delete(function(req, res) {
        Shop.findById(req.params.id, function(err, shop) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                Robot.findById(req.params.rid, function(err, robot) {
                    try {

                        robot.remove(function(err) {
                            if (err) {
                                res.status(500).send(err);
                            }
                            else {
                                var ret = ""
                                res.json({ status: "ok" });
                            }
                        });
                    } catch (err) {
                        res.status(500).send(err);
                    }
                });
            }
        });
    });

appRouter.route('/shop/:id/execute')
    .post(function(req, res) {
        Shop.findById(req.params.id, function(err, shop) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                Robot.find({ shopId: shop._id }, function(err, robots) {
                    if (err) {
                        res.status(500).send(err);
                    }
                    else {
                        if (robots.length < 1) {
                            res.send({ status: "No robot in the system!" });
                            return;
                        }
                        var robots_move_obj = new robotsMove();
                        for (var i = 0; i < robots.length; i++) {
                            robots[i].shop = shop;
                        }
                        var s = {};
                        s["id"] = shop._id;
                        s["width"] = shop.width;
                        s["height"] = shop.height;
                        var updated_robots = robots_move_obj.updateLocationAll(robots);
                        s["robots"] = updated_robots.robots;
                        // s["robots"] = robots_move_obj.updateLocationAll(robots);
                        if (updated_robots.hasclash) {
                            console.log('Clash happens! Robots don\'t move');
                            res.send({ status: 'Clash happens! Robots don\'t move' });
                        }
                        else {
                            console.log(s);
                            res.send(s);
                        }

                    }
                });
            }
        });
    });


app.use('/', appRouter);
app.listen(port, function() {
    console.log('process.env.MONGO_URL', process.env.MONGO_URL);
    console.log('running on port: ', port);
})
