# HeyYou Coding Test - Coffee Robots

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

This program returns the last location of the robots given an input.txt file

## Online demo

This should be viewable through to the following link:

`https://secret-brook-74373.herokuapp.com`
 
## Setup

**This program was developed with Node.js v5.0.0**

All you need to do to setup is:

    npm install

MONGO_URL needs to be exported before running commands.

    export MONGO_URL=mongodb://heyyou:<dbpassword>@ds025449.mlab.com:25449/heyyou

## Usage

    usage: ./script.js <file>

    <file> - txt file containing robots location and shop size.


Example: Simply run 

    > node script.js input.txt

There is a sample input txt at `./test/valid.txt` so as an example we can run:

	5 5
	1 2 N 
	LMLMLMLMM 
	3 3 E 
	MLMLMRMRMRRM

## Running on server
Source codes are found under `src/`.

    > nodemon server.js

## Testing

Tests are found under `test/`.

In order to execute test harness run:

    node_modules/mocha/bin/mocha

## Debugging

    > node_modules/mocha/bin/mocha --debug-brk
    
Then attach Visual studio code debugger.

## Assumptions

- Input and output CSVs do **not** include header rows
- Output is printed to STDOUT so user can simply pipe to a csv file e.g. `./script.js myinput.txt > output.csv`
- All errors are printed to STDERR
- If any clash happens, the program does alert and all robots stay still on their initial location
- Using third party libs is permitted
- Using mongo data store is permitted.